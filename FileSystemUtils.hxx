#ifndef FILE_SYSTEM_UTILS_HXX
#define FILE_SYSTEM_UTILS_HXX

#include <string>
#include <boost/filesystem.hpp>

class FileSystemUtils
{
public:
  bool md(const std::string &);
  bool rd(const std::string &);
  bool ed(const std::string &);
};

#endif
