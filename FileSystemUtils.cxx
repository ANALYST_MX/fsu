#include "FileSystemUtils.hxx"

bool FileSystemUtils::md(const std::string &path)
{
  if ( ed(path) )
    {
      return false;
    }
  
  return boost::filesystem::create_directories(path);
}

bool FileSystemUtils::rd(const std::string &path)
{
  if ( !ed(path) )
    {
      return false;
    }
  
  return boost::filesystem::remove_all(path);
}

bool FileSystemUtils::ed(const std::string &path)
{
  return boost::filesystem::exists(path);
}
