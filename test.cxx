#include "test.hxx"

int main(const int argc, const char *argv[])
{
  assertFileSystemUtils();
  
  return EXIT_SUCCESS;
}

void assertFileSystemUtils()
{
  std::cout << "\t\033[1;34mstart check FileSystemUtils\033[0m" << std::endl;
  FileSystemUtils fsu;
  info("check md");
  fsu.md("/tmp/tFileSystemUtilsTest");
  assert(fsu.ed("/tmp/tFileSystemUtilsTest"));
  ok();
  info("check ed");
  assert(fsu.ed("/tmp/tFileSystemUtilsTest"));
  ok();
  info("check rd");
  fsu.rd("/tmp/tFileSystemUtilsTest");
  assert(!fsu.ed("/tmp/tFileSystemUtilsTest"));
  ok();
  std::cout << "\t\033[1;34mend check FileSystemUtils\033[0m" << std::endl;
}

void ok()
{
  std::cout << "\033[1;32mok\033[0m" << std::endl;
}

void info(const char *msg)
{
  std::cout << msg << ": ";
}
