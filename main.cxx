#include "main.hxx"

int main(const int argc, const char *argv[])
{
  if( argc < 3 )
    {
      usage(argv[0]);
      return EXIT_SUCCESS;
    }
  
  std::shared_ptr<FileSystemUtils> fsu (new FileSystemUtils);

  std::shared_ptr< std::map< std::string, int (*)(FileSystemUtils &, const std::string) > > m (new std::map< std::string, int (*)(FileSystemUtils &, const std::string) >);
  m->insert(std::pair< std::string, int (*)(FileSystemUtils &, const std::string) >("-md", md));
  m->insert(std::pair< std::string, int (*)(FileSystemUtils &, const std::string) >("-rd", rd));
  m->insert(std::pair< std::string, int (*)(FileSystemUtils &, const std::string) >("-ed", ed));
  
  auto it = m->find(argv[1]);
  if (it != m->end())
    {
      it->second(*fsu, argv[2]);
      return EXIT_SUCCESS;
    }

  usage(argv[0]);
  return EXIT_FAILURE;
}

void usage(const char *argv)
{
  std::cout << "Usage: " << argv << " -[md|rd|ed] <src_path>" << std::endl;
}

int md(FileSystemUtils &fsu, const std::string path)
{
  if ( fsu.md(path) )
    {
      return EXIT_SUCCESS;
    }
  return EXIT_FAILURE;
}

int rd(FileSystemUtils &fsu, const std::string path)
{
  if ( fsu.rd(path) )
    {
      return EXIT_SUCCESS;
    }
  return EXIT_FAILURE;
}

int ed(FileSystemUtils &fsu, const std::string path)
{
  if ( fsu.ed(path) )
    {
      return EXIT_SUCCESS;
    }
  return EXIT_FAILURE;
}
