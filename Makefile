# Compiler
GXX := g++
GXXFLAG := -std=c++11 -Wall
MAIN := main
TEST := test
CXX := .cxx
HXX := .hxx
OBJ := .o
OBJ_LIST := FileSystemUtils$(OBJ)
LIB_LIST := -lboost_system -lboost_filesystem
.PHONY: all
all: $(MAIN) $(TEST)
	@ echo Build complete
$(MAIN): $(MAIN)$(OBJ) $(OBJ_LIST)
	$(GXX) $(GXXFLAG) $(MAIN)$(OBJ) $(OBJ_LIST) -o $(MAIN) $(LIB_LIST)
$(MAIN)$(OBJ): $(MAIN)$(CXX) $(MAIN)$(HXX)
	$(GXX) $(GXXFLAG) -c $(MAIN)$(CXX) -o $(MAIN)$(OBJ)
$(TEST): $(TEST)$(OBJ) $(OBJ_LIST)
	$(GXX) $(GXXFLAG) $(TEST)$(OBJ) $(OBJ_LIST) -o $(TEST) $(LIB_LIST)
$(TEST)$(OBJ): $(TEST)$(CXX) $(TEST)$(HXX)
	$(GXX) $(GXXFLAG) -c $(TEST)$(CXX) -o $(TEST)$(OBJ)
FileSystemUtils$(OBJ): FileSystemUtils$(CXX) FileSystemUtils$(HXX)
	$(GXX) $(GXXFLAG) -c FileSystemUtils$(CXX) -o FileSystemUtils$(OBJ)
.PHONY: clean check
clean:
	rm -rf $(MAIN) $(TEST) *$(OBJ) *~
check:
	./$(TEST)
