#ifndef MAIN_HXX
#define MAIN_HXX

#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <cstring>
#include "FileSystemUtils.hxx"

void usage(const char *argv);
int md(FileSystemUtils &, const std::string);
int rd(FileSystemUtils &, const std::string);
int ed(FileSystemUtils &, const std::string);

#endif
